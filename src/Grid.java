
import java.awt.*;
import java.util.Random;

public class Grid {

    Cell[][] cells = new Cell[20][20];
    int[] d = new int[]{3,5,7,9};
    static Random randNum = new Random();
    Water w = new Water();
    Mauntain m = new Mauntain();
    Road r = new Road();
    Grass g;
    Actors[] actor = new Actors[]{w,m,r};
    public Grid(){
         int r = randNum.nextInt(d.length);
        System.out.println(r);
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if((i&j)% d[r] == 0){
                    g = new Grass();
                    cells[i][j] = new Cell(10+35 *i, 10+35*j,g);
                }else{
                    cells[i][j] = new Cell(10+35 *i, 10+35*j,getRandom(actor));
                }
                
            }
        }
    }

    public void paint(Graphics g, Point mousePos){
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                cells[i][j].paint(g, mousePos);
                
            }
        }
    }
    public static Actors getRandom(Actors[] array) {
        int rnd = new Random().nextInt(array.length);
    
        return array[rnd];
    }

}