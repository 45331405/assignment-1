import java.awt.*;

public class Cell extends Rectangle{


    static int size = 35;
    Actors a;
    Info info;

    public Cell(int x , int y, Actors a){
        
        super(x, y ,size,size);
        this.a = a;
        
    }

    
    public void paint (Graphics g, Point mousepos){
        
        g.setColor(a.c);
        g.fillRect(x, y, size, size);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, size, size);
        g.drawString(" The type of Tile : ", 730 + 10, 20 + 30);
        g.drawString("Movement Cost : ", 730 + 10, 20 + 50);
        if (isMouseIN(mousepos)) {
            g.setColor(Color.white);
            g.drawRect(x+1, y+1, size -2, size -2);

            g.setColor(Color.black);
            g.drawString(" The type of Tile : " + a.type, 730 + 10, 20 + 30);
            g.drawString("Movement Cost : " + Integer.toString(a.movemnet) , 730 + 10, 20 + 50);
            
        }
    }


    boolean isMouseIN( Point  p){

        if (p!= null) {
            return(x < p.x && x+size >p.x && y < p.y && y+size > p.y );
            
        }else{
            return false;
        }
    }
}