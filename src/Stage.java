import java.awt.*;  
public class Stage{

    Grid grid;
    Info info;
    
    public Stage(){

        grid = new Grid();
        info = new Info();
    }

    public void paint(Graphics g, Point mousPos){

        grid.paint(g , mousPos);
        info.paint(g);
    }

    

}